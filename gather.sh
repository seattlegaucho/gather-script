#!/bin/bash
# Prototype script to gather information on Linux systems

# Where to gather all the info
TS=$(date +%Y%m%d-%H%M%S)
TEMP_DIR="/tmp/SkySQL_$TS"
mkdir -pv $TEMP_DIR

# Release information
lsb_release -a >$TEMP_DIR/release.txt

# Hardware information
# Command is deprecated in Ubuntu 13.10 and higher, need to find an alternative.
hwinfo --cpu --disk --memory >$TEMP_DIR/hwinfo.txt

# System logs
# For Ubuntu and similar systems use the line below
cp /var/log/syslog $TEMP_DIR/syslog_$TS
# For RH/CentOS systems use the line below
# cp /var/log/messages $TEMP_DIR/messages_$TS

# MySQL error log
# Need to be automated
# mysql -e "show global variables like '%log%err%'"
MYSQL_ERR='/var/lib/mysql/homer.err'
cp $MYSQL_ERR $TEMP_DIR/

# Running processes
top -b -n 1 > $TEMP_DIR/top_$(date +%Y%m%d-%H%M).txt

# Basic 'sar' runs
# Needs to verify if 'sar' is present in the system
sar -Bru >$TEMP_DIR/sar_$(date +%Y%m%d-%H%M).txt

# NUMA information
# Needs to verify if numactl is installed
numactl --hardware > $TEMP_DIR/numa.txt

# Filesystems
cp /etc/fstab $TEMP_DIR/
df -h > $TEMP_DIR/df.txt

# Top 5 memory using processes
ps -eo rss,pid,user,command | sort -rn | head -5 | awk '{ hr[1024^2]="GB"; hr[1024]="MB"; for (x=1024^3; x>=1024; x/=1024) { if ($1>=x) { printf ("%-6.2f %s ", $1/x, hr[x]); break } } } { printf ("%-6s %-10s ", $2, $3) } { for ( x=4 ; x<=NF ; x++ ) { printf ("%s ",$x) } print }' > $TEMP_DIR/memory_$(date +%Y%m%d-%H%M).txt

# Start gathering MySQL information
# It assumes credentials are provided in ~/.my.cnf file
mysqladmin -i5 -r -c12 ext > $TEMP_DIR/mysqladmin_$(date +%Y%m%d-%H%M).txt

# Database schemas
# Database schemas need to be provided
mysqldump --no-data my_test > $TEMP_DIR/schemas.sql

# Summary info from the I_S
SQL_CMD="SET GLOBAL innodb_stats_on_metadata = OFF;
select table_schema, engine, count(*), sum(data_length), sum(index_length), avg(table_rows) from information_schema.tables where table_schema not in ('information_schema', 'performance_schema', 'mysql') group by table_schema, engine"
mysql -e "$SQL_CMD" > $TEMP_DIR/IS_summary.txt

# Create a tarball
tar cvzf ~/SkySQL-$(date +%Y%m%d-%H%M).tgz $TEMP_DIR/*
